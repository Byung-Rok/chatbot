#-*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

from slack.web.classes import extract_json
from slack.web.classes.blocks import *

SLACK_TOKEN = "xoxb-689176898790-691875809846-KlTchAAZSA4mTBpZzx5rVTvm"
SLACK_SIGNING_SECRET = "8cf6249a925181a26ab60799de78c395"


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

is_remention = False    # 두번째로 들어오는 메시지인지 판별하는 is_remention 변수
href=[]

def _crawl_food_keywords(text):
    #data
    print(text)
    global is_remention
    global href
    keywords = []
    max_recipe = 10
    keyword = text.strip().split()[1]
    key = ""
    # 첫번째 메시지가 들어올 때    # 요리를 검색해서 레시피 목록을 출력
    if is_remention == False:
        href = []
        for t in text.split()[1:]:
            key += t + " "
        url = "http://www.10000recipe.com/recipe/list.html?q=" + urllib.parse.quote_plus(key) + "&order=reco"
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")
        ul = soup.find("div", class_="rcp_m_list2")
        for i, div in enumerate(ul.find_all("div", class_="col-xs-4")):
            if i<max_recipe :
                href.append("http://www.10000recipe.com"+div.find("a")["href"])
                form = "*" + str(i+1) + ". " +div.find("h4", class_="ellipsis_title2").get_text().replace("*",'') + "*   _" + div.find("p").get_text() + "_ <" + href[i] + "|바로가기>"
                keywords.append(form)
        if not keywords:
            return '검색 결과를 찾을 수 없습니다.\n다시 @chatbot *<음식>* 입력해주세요.'
        else:
            is_remention = True
            keywords.append("\n\n`원하시는 레시피 번호를 입력해주세요 (0 : 뒤로가기)`")
            return u'\n'.join(keywords)

    # 두번째 메시지가 들어올 때    # 들어오는 입력값에 따라 레시피 출력할지말지 정함
    else:
        index = int(keyword)-1
        if re.compile('[\d\d?]').match(keyword): #입력이 잘 된 경우
            if index == -1:
                is_remention = False
                return "\n뒤로가기\n@chatbot *<음식>* 입력해주세요."
            elif index > max_recipe or index < -1:
                return "1~"+ str(max_recipe) +" 중 선택해주세요"
            else: #메인 동작 1~max_data가 입력됨
                block1 = None 
                block2 = None
                print(href)
                url = href[index]
                msg = ""
                source_code = urllib.request.urlopen(url).read()
                soup = BeautifulSoup(source_code, "html.parser")
               #--------------------------------url------------------------------------
                ingre = soup.find("div", class_="ready_ingre3")
                for ul in ingre.find_all("ul"):
                    ingre_form = "*" + ul.find("b").get_text() + "*\n"
                    for i,li in enumerate(ul.find_all("li")):
                        detail_ingre = li.get_text().strip().split()
                        if len(detail_ingre) > 1:
                            preprc = detail_ingre[0] + " / _" + detail_ingre[1] + "_,\t"
                            ingre_form += preprc if (i+1)!=len(ul.find_all("li")) else preprc.replace(",", '')
                        else:
                            ingre_form += detail_ingre[0] + "\t"
                    msg+=ingre_form+"\n"
                msg+="\n\n\n\n"
                #---------------------------------재료 ---------------------------
                step = soup.find("div", class_="view_step")
                for i, cook in enumerate(step.find_all("div", class_= "media-body")):
                    msg += "*" + str(i+1) + ".* " + cook.get_text().replace('\n','')  + "\n\n"
                #---------------조리순서-----------------------
                #carousel-inner
                carousel = soup.find("div", class_="view2_pic")
                inner = carousel.find("img")["src"]
                print(inner)
                #---------------대표이미지---------------------
                block1 =SectionBlock(text=msg)
                block2 =ImageBlock(image_url = inner, alt_text="~이미지를 불러오지 못하였습니다~")
                mblocks = [block1, block2]
                #레시피 종료
                is_remention = False
                return mblocks
        else : #TO-BE 한글이나 형식에 안맞는 데이터가 입력되었을 경우
            return "@chattbot <원하는 레시피 넘버>(숫자만 입력해주세요.)"
    


# 챗봇이 멘션을 받았을 경우     
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    keywords = _crawl_food_keywords(text)
    if(type(keywords)==list):
        slack_web_client.chat_postMessage(
            channel=channel,
            blocks=extract_json(keywords)
        )
    else:
        slack_web_client.chat_postMessage(
            channel =channel,
            text = keywords
        )

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)